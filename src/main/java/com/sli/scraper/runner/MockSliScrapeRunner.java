package com.sli.scraper.runner;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sli.scraper.entity.ProductScrapeResponse;
import com.sli.scraper.task.SliScrapeJobRunner;

/* Test class reading urls from a static file and fetching product info for each url*/
public class MockSliScrapeRunner {
	private static Logger logger = LoggerFactory.getLogger(MockSliScrapeRunner.class);

	private static final String URL_FILE_LOCATION = "file/sli-demo-urls.txt";

	public static void main(String[] args) {
		MockSliScrapeRunner runner = new MockSliScrapeRunner();
		runner.run();
	}

	private void run() {
		List<String> urls = new ArrayList<>();

		ClassLoader classLoader = getClass().getClassLoader();
		File file = new File(classLoader.getResource(URL_FILE_LOCATION).getFile());
		Scanner scanner = null;
		try {
			scanner = new Scanner(file);
			while (scanner.hasNextLine()) {
				urls.add(scanner.nextLine());
			}
		} catch (FileNotFoundException e) {
			logger.error("SLI Url file not found.");
		} finally {
			if (scanner != null) {
				scanner.close();
			}
		}
		long start = System.currentTimeMillis();
		List<ProductScrapeResponse> responseList = SliScrapeJobRunner.scrapeSliUrls(urls);
		for (ProductScrapeResponse response : responseList) {
			logger.info(response.toString());
			logger.info("------------------------------------------------------------------------");
		}

		StringBuilder log = new StringBuilder("Total time taken to parse ").append(urls.size()).append(" urls : ")
				.append(System.currentTimeMillis() - start).append(" millisec").append("\n");
		logger.info(log.toString());
	}

}
