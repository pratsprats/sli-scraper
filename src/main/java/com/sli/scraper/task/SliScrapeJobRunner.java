package com.sli.scraper.task;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sli.scraper.constants.ScraperConstants;
import com.sli.scraper.entity.ProductScrapeRequest;
import com.sli.scraper.entity.ProductScrapeResponse;

/* Class responsible for executing all requests in different threads using executor service */
public class SliScrapeJobRunner {
	private static Logger logger = LoggerFactory.getLogger(SliScrapeJobRunner.class);

	public static List<ProductScrapeResponse> scrapeSliUrls(List<String> urls) {
		CountDownLatch count = new CountDownLatch(urls.size());
		List<ProductScrapeResponse> scrapeResults = new ArrayList<>();

		List<Future<ProductScrapeResponse>> futureList = new ArrayList<>();
		ExecutorService executorService = Executors.newFixedThreadPool(ScraperConstants.NUM_OF_THREADS);

		try {
			for (String url : urls) {
				if (url != null && !url.trim().isEmpty()) {
					SliScrapeJob scrapeJob = new SliScrapeJob(count, new ProductScrapeRequest(url.trim()));
					Future<ProductScrapeResponse> futureResponse = executorService.submit(scrapeJob);
					futureList.add(futureResponse);
				} else {
					count.countDown();
				}
			}

			try {
				count.await();
			} catch (InterruptedException e) {
				logger.error("Some error occurred while waiting for threads to complete the task.", e);
			}

			for (Future<ProductScrapeResponse> futureResponse : futureList) {
				try {
					scrapeResults.add(futureResponse.get());
				} catch (InterruptedException | ExecutionException e) {
					logger.error("Some error occurred while fetching result from future.", e);
				}
			}
		} finally {
			executorService.shutdown();
		}

		return scrapeResults;

	}

}
