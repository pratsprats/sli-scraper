package com.sli.scraper.task;

import static com.sli.scraper.constants.ScraperConstants.ACCEPT_ENCODING_HEADER;
import static com.sli.scraper.constants.ScraperConstants.ACCEPT_ENCODING_HEADER_VALUE;
import static com.sli.scraper.constants.ScraperConstants.BREADCRUMB_DELIMITER;
import static com.sli.scraper.constants.ScraperConstants.DUMMY_USER_AGENT;
import static com.sli.scraper.constants.ScraperConstants.FRONTEND_HEADER;
import static com.sli.scraper.constants.ScraperConstants.IN_STOCK;
import static com.sli.scraper.constants.ScraperConstants.PAGE_FETCH_TIMEOUT;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sli.scraper.entity.Product;
import com.sli.scraper.entity.ProductPricing;
import com.sli.scraper.entity.ProductScrapeRequest;
import com.sli.scraper.entity.ProductScrapeResponse;

/* Class responsible for scraping all product info and attributes given SLI demo product url */
public class SliScraper {
	private static Logger logger = LoggerFactory.getLogger(SliScraper.class);

	private ProductScrapeRequest productScrapeRequest;

	public SliScraper(ProductScrapeRequest productScrapeRequest) {
		this.productScrapeRequest = productScrapeRequest;
	}

	public ProductScrapeResponse extractProductInfo() {
		ProductScrapeResponse productScrapeResponse = new ProductScrapeResponse(productScrapeRequest.getUrl());
		Product product = new Product();

		try {
			Document doc = getDocument(productScrapeRequest.getUrl());
			if (doc != null) {
				// logger.info("HTML body for url - " +
				// productScrapeRequest.getUrl() + " : " + doc.body().html());
				Elements productInfoTopDiv = doc.select("div.product-shop");

				if (productInfoTopDiv != null) {
					product.setName(extractProductName(productInfoTopDiv));
					product.setInStock(extractProductAvailability(productInfoTopDiv));
					product.setShortDescription(extractShortDescription(productInfoTopDiv));
					product.setPricing(extractPricing(productInfoTopDiv));
				}
				product.setCategoryBreadcrumbs(extractCategoryBreadcrumbs(doc));
				product.setAttributes(extractProductAttributes(doc));
				product.setDescription(extractProductDescription(doc));
			}
		} catch (Exception exception) {
			logger.error(
					"Some error occurred while extracting product info from url : " + productScrapeRequest.getUrl(),
					exception);
		}

		productScrapeResponse.setScrapeEndTime(new Date());
		productScrapeResponse.setProduct(product);
		return productScrapeResponse;
	}

	private Document getDocument(String url) throws IOException {
		Document doc = Jsoup.connect(url).userAgent(DUMMY_USER_AGENT)
				.header(ACCEPT_ENCODING_HEADER, ACCEPT_ENCODING_HEADER_VALUE).maxBodySize(0).timeout(PAGE_FETCH_TIMEOUT)
				.cookie(FRONTEND_HEADER, "nr70oo48es142eu22httio5m96").get();
		return doc;
	}

	private String extractShortDescription(Elements productInfoTopDiv) {
		String shortDescription = null;
		Elements shortDescElement = productInfoTopDiv.select("div.short-description div.std");
		if (isNotNullNotEmpty(shortDescElement)) {
			shortDescription = shortDescElement.text().trim();
		}
		return shortDescription;
	}

	private Boolean extractProductAvailability(Elements productInfoTopDiv) {
		Boolean isAvailable = null;
		Elements availabilityElement = productInfoTopDiv.select("div.extra-info p.availability span.value");
		if (isNotNullNotEmpty(availabilityElement)) {
			isAvailable = availabilityElement.text().trim().equalsIgnoreCase(IN_STOCK);
		}
		return isAvailable;
	}

	private String extractProductName(Elements productInfoTopDiv) {
		String productName = null;
		Elements productNameElement = productInfoTopDiv.select("div.product-name span.h1");
		if (isNotNullNotEmpty(productNameElement)) {
			productName = productNameElement.text().trim();
		}
		return productName;
	}

	private ProductPricing extractPricing(Elements productInfoTopDiv) {
		ProductPricing pricing = new ProductPricing();

		Elements priceInfoElement = productInfoTopDiv.select("div.price-info");
		if (priceInfoElement != null) {
			Elements priceBoxElement = priceInfoElement.select("div.price-box");
			if (priceBoxElement != null) {
				/*
				 * Basic Assumption - Either regular price or old price tags
				 * will be available
				 */
				Elements regularPriceElement = priceBoxElement.select("*.regular-price span.price");
				if (isNotNullNotEmpty(regularPriceElement)) {
					pricing.setRegularPrice(regularPriceElement.text().trim());
				}

				Elements oldPriceElement = priceBoxElement.select("*.old-price span.price");
				if (isNotNullNotEmpty(oldPriceElement)
						&& (pricing.getRegularPrice() == null || pricing.getRegularPrice().isEmpty())) {
					pricing.setRegularPrice(oldPriceElement.text().trim());
				}

				Elements specialPriceElement = priceBoxElement.select("*.special-price span.price");
				if (isNotNullNotEmpty(specialPriceElement)) {
					pricing.setDiscountedPrice(specialPriceElement.text().trim());
				}

				/*
				 * To handle cases like this -
				 * http://www.sli-demo.com/home-decor/electronics/mp3-player-
				 * with-audio.html, fetching toprice, fromprice and
				 * configuredprice
				 */
				Elements fromPriceElement = priceBoxElement.select("*.price-from span.price");
				if (isNotNullNotEmpty(fromPriceElement)) {
					pricing.setFromPrice(fromPriceElement.text().trim());
				}

				Elements toPriceElement = priceBoxElement.select("*.price-to span.price");
				if (isNotNullNotEmpty(toPriceElement)) {
					pricing.setToPrice(toPriceElement.text().trim());
				}

				Elements configuredPriceElement = priceBoxElement
						.select("*.price-as-configured span.full-product-price span.price");
				if (isNotNullNotEmpty(configuredPriceElement)) {
					pricing.setConfiguredPrice(configuredPriceElement.text().trim());
				}

			}
		}
		return pricing;
	}

	private List<String> extractCategoryBreadcrumbs(Document document) {
		List<String> categoryBreadcrumbs = new ArrayList<String>();

		Elements breadcrumbs = document.getElementsByClass("breadcrumbs");
		if (breadcrumbs != null) {
			Elements liElement = breadcrumbs.select("li");
			if (isNotNullNotEmpty(liElement)) {
				String text = liElement.text().trim();
				if (text != null) {
					String[] breadcrumbArray = text.split(BREADCRUMB_DELIMITER);
					for (String individualBreadcrumb : breadcrumbArray) {
						categoryBreadcrumbs.add(individualBreadcrumb.trim());
					}
				}
			}
		}
		return categoryBreadcrumbs;
	}

	private String extractProductDescription(Document document) {
		String productDescription = null;
		Elements tabContainerElement = document.getElementsByClass("tab-container");
		if (tabContainerElement != null && tabContainerElement.first() != null) {
			Elements productDescriptionElement = tabContainerElement.first().select("div.tab-content div.std");
			if (isNotNullNotEmpty(productDescriptionElement)) {
				productDescription = productDescriptionElement.text().trim();
			}
		}
		return productDescription;
	}

	private Map<String, String> extractProductAttributes(Document document) {
		Map<String, String> attributes = new HashMap<String, String>();
		Elements tableElement = document.select("table#product-attribute-specs-table tr");
		if (tableElement != null) {
			for (Element element : tableElement) {
				Elements thElements = element.getElementsByTag("th");
				Element thElement = thElements.first();
				Elements tdElements = element.getElementsByTag("td");
				Element tdElement = tdElements.first();
				attributes.put(thElement.text().trim(), tdElement.text().trim());
			}
		}
		return attributes;
	}

	private boolean isNotNullNotEmpty(Elements element) {
		return element != null && !element.text().trim().isEmpty();
	}

	public ProductScrapeRequest getProductScrapeRequest() {
		return productScrapeRequest;
	}
}
