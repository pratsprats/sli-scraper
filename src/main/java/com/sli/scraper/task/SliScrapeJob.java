package com.sli.scraper.task;

import java.util.concurrent.Callable;
import java.util.concurrent.CountDownLatch;

import com.sli.scraper.entity.ProductScrapeRequest;
import com.sli.scraper.entity.ProductScrapeResponse;

public class SliScrapeJob implements Callable<ProductScrapeResponse> {

	private CountDownLatch count;
	private ProductScrapeRequest request;

	public SliScrapeJob(CountDownLatch count, ProductScrapeRequest request) {
		this.count = count;
		this.request = request;
	}

	public ProductScrapeResponse call() {
		try {
			SliScraper scraper = new SliScraper(request);
			return scraper.extractProductInfo();
		} finally {
			count.countDown();
		}
	}

}
