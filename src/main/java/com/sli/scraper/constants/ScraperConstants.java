package com.sli.scraper.constants;

public interface ScraperConstants {
	public static final String DUMMY_USER_AGENT = "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.84 Safari/537.36";
	public static final String ACCEPT_ENCODING_HEADER = "Accept-Encoding";
	public static final String ACCEPT_ENCODING_HEADER_VALUE = "gzip, deflate";
	public static final int PAGE_FETCH_TIMEOUT = 10000;
	public static final String FRONTEND_HEADER = "frontend";
	public static final String IN_STOCK = "In stock";
	public static final String BREADCRUMB_DELIMITER = "/";
	public static final int NUM_OF_THREADS = 10;

}
