package com.sli.scraper.entity;

import java.util.Date;

public class ProductScrapeResponse {

	private Product product;
	private Date scrapeStartTime;
	private Date scrapeEndTime;
	private String url;

	public ProductScrapeResponse(String url) {
		scrapeStartTime = new Date();
		this.url = url;
	}

	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public Date getScrapeStartTime() {
		return scrapeStartTime;
	}

	public void setScrapeStartTime(Date scrapeStartTime) {
		this.scrapeStartTime = scrapeStartTime;
	}

	public Date getScrapeEndTime() {
		return scrapeEndTime;
	}

	public void setScrapeEndTime(Date scrapeEndTime) {
		this.scrapeEndTime = scrapeEndTime;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append("Url - ").append(url);

		if (product != null) {
			result.append(product.toString());
		}

		result.append("\n");
		result.append("Scrape start time - ").append(scrapeStartTime);

		result.append("\n");
		result.append("Scrape end time - ").append(scrapeEndTime);

		return result.toString();
	}

}
