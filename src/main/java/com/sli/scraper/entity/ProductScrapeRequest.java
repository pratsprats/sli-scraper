package com.sli.scraper.entity;

import java.util.Date;

public class ProductScrapeRequest {

	private String url;
	private short retryCount;
	private Date lastRetryDate;
	private Date submitDate;

	public ProductScrapeRequest(String url) {
		this.url = url;
		retryCount = 1;
		this.lastRetryDate = new Date();
		this.submitDate = new Date();
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public short getRetryCount() {
		return retryCount;
	}

	public void setRetryCount(short retryCount) {
		this.retryCount = retryCount;
	}

	public Date getLastRetryDate() {
		return lastRetryDate;
	}

	public void setLastRetryDate(Date lastRetryDate) {
		this.lastRetryDate = lastRetryDate;
	}

	public Date getSubmitDate() {
		return submitDate;
	}

	public void setSubmitDate(Date submitDate) {
		this.submitDate = submitDate;
	}

}
