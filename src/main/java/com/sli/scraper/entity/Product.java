package com.sli.scraper.entity;

import java.util.List;
import java.util.Map;

public class Product {

	private String name;
	private ProductPricing pricing;
	private List<String> categoryBreadcrumbs;
	private String description;
	private String shortDescription;
	private Boolean inStock;
	private Map<String, String> attributes;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<String> getCategoryBreadcrumbs() {
		return categoryBreadcrumbs;
	}

	public void setCategoryBreadcrumbs(List<String> categoryBreadcrumbs) {
		this.categoryBreadcrumbs = categoryBreadcrumbs;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public Boolean isInStock() {
		return inStock;
	}

	public void setInStock(Boolean inStock) {
		this.inStock = inStock;
	}

	public Map<String, String> getAttributes() {
		return attributes;
	}

	public void setAttributes(Map<String, String> attributes) {
		this.attributes = attributes;
	}

	public ProductPricing getPricing() {
		return pricing;
	}

	public void setPricing(ProductPricing pricing) {
		this.pricing = pricing;
	}

	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();

		if (name != null && !name.isEmpty()) {
			result.append("\n");
			result.append("Product Name - ").append(name);
		}

		if (shortDescription != null && !shortDescription.isEmpty()) {
			result.append("\n");
			result.append("Short Description - ").append(shortDescription);
		}

		if (description != null && !description.isEmpty()) {
			result.append("\n");
			result.append("Description - ").append(description);
		}

		if (categoryBreadcrumbs != null && !categoryBreadcrumbs.isEmpty()) {
			result.append("\n");
			result.append("Category - ").append(categoryBreadcrumbs);
		}

		if (inStock != null) {
			result.append("\n");
			result.append("In Stock - ").append(inStock);
		}

		if (attributes != null && !attributes.isEmpty()) {
			result.append("\n");
			result.append("Attributes - ").append(attributes);
		}

		if (pricing != null) {
			if (pricing.getRegularPrice() != null && !pricing.getRegularPrice().isEmpty()) {
				result.append("\n");
				result.append("Regular Price - ").append(pricing.getRegularPrice());
			}
			if (pricing.getDiscountedPrice() != null && !pricing.getDiscountedPrice().isEmpty()) {
				result.append("\n");
				result.append("Discounted Price - ").append(pricing.getDiscountedPrice());
			}
			if (pricing.getFromPrice() != null && !pricing.getFromPrice().isEmpty()) {
				result.append("\n");
				result.append("From Price - ").append(pricing.getFromPrice());
			}
			if (pricing.getToPrice() != null && !pricing.getToPrice().isEmpty()) {
				result.append("\n");
				result.append("To Price - ").append(pricing.getToPrice());
			}
			if (pricing.getConfiguredPrice() != null && !pricing.getConfiguredPrice().isEmpty()) {
				result.append("\n");
				result.append("Configured Price - ").append(pricing.getConfiguredPrice());
			}
		}

		return result.toString();
	}

}
