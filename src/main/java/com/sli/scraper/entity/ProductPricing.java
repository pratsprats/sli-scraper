package com.sli.scraper.entity;

public class ProductPricing {
	private String regularPrice;
	private String discountedPrice;

	private String fromPrice;
	private String toPrice;
	private String configuredPrice;

	public String getRegularPrice() {
		return regularPrice;
	}

	public void setRegularPrice(String regularPrice) {
		this.regularPrice = regularPrice;
	}

	public String getDiscountedPrice() {
		return discountedPrice;
	}

	public void setDiscountedPrice(String discountedPrice) {
		this.discountedPrice = discountedPrice;
	}

	public String getFromPrice() {
		return fromPrice;
	}

	public void setFromPrice(String fromPrice) {
		this.fromPrice = fromPrice;
	}

	public String getToPrice() {
		return toPrice;
	}

	public void setToPrice(String toPrice) {
		this.toPrice = toPrice;
	}

	public String getConfiguredPrice() {
		return configuredPrice;
	}

	public void setConfiguredPrice(String configuredPrice) {
		this.configuredPrice = configuredPrice;
	}

}
