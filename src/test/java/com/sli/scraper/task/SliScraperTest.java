package com.sli.scraper.task;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.sli.scraper.entity.ProductPricing;
import com.sli.scraper.entity.ProductScrapeRequest;
import com.sli.scraper.entity.ProductScrapeResponse;

import mockit.Deencapsulation;
import mockit.Mock;
import mockit.MockUp;

public class SliScraperTest {

	@Test
	public void testExtractShortDescriptionSuccess() {
		String expectedOutput = "A bold hue and understated dobby detail bring refined nuance "
				+ "to this modern dress shirt.";
		String html = "<div class=\"product-shop\"><div class=\"short-description\">"
				+ "<div class=\"std\">A bold hue and understated dobby detail bring refined nuance "
				+ "to this modern dress shirt.</div> </div></div>";
		Document doc = Jsoup.parse(html);
		SliScraper scraper = new SliScraper(new ProductScrapeRequest("/someTestUrl"));
		String shortDesc = Deencapsulation.invoke(scraper, "extractShortDescription", doc.getAllElements());
		Assert.assertEquals(shortDesc, expectedOutput);
	}

	@Test
	public void testExtractShortDescriptionFailure() {
		String html = "<div class=\"product-shop\"><div class=\"short-descriptio\">"
				+ "<div class=\"std\">A bold hue and understated dobby detail bring refined nuance "
				+ "to this modern dress shirt.</div> </div></div>";
		Document doc = Jsoup.parse(html);
		SliScraper scraper = new SliScraper(new ProductScrapeRequest("/someTestUrl"));
		String shortDesc = Deencapsulation.invoke(scraper, "extractShortDescription", doc.getAllElements());
		Assert.assertNull(shortDesc);
	}

	@Test
	public void testExtractProductNameSuccess() {
		String expectedOutput = "Slim fit Dobby Oxford Shirt";
		String html = "<div class=\"product-shop\"><div class=\"product-name\">"
				+ "<span class=\"h1\">Slim fit Dobby Oxford Shirt</span></div></div>";
		Document doc = Jsoup.parse(html);
		SliScraper scraper = new SliScraper(new ProductScrapeRequest("/someTestUrl"));
		String shortDesc = Deencapsulation.invoke(scraper, "extractProductName", doc.getAllElements());
		Assert.assertEquals(shortDesc, expectedOutput);
	}

	@Test
	public void testExtractProductNameFailure() {
		String html = "<div class=\"product-shop\"><div class=\"product-name\">"
				+ "<span class=\"h2\">Slim fit Dobby Oxford Shirt</span></div></div>";
		Document doc = Jsoup.parse(html);
		SliScraper scraper = new SliScraper(new ProductScrapeRequest("/someTestUrl"));
		String shortDesc = Deencapsulation.invoke(scraper, "extractProductName", doc.getAllElements());
		Assert.assertNull(shortDesc);
	}

	@Test
	public void testExtractProductAvailabilityInStock() {
		String html = "<div class=\"product-shop\"><div class=\"extra-info\">"
				+ "<p class=\"availability in-stock\"><span class=\"label\">"
				+ "Availability:</span> <span class=\"value\">In stock</span></p></div></div>";
		Document doc = Jsoup.parse(html);
		SliScraper scraper = new SliScraper(new ProductScrapeRequest("/someTestUrl"));
		Boolean inStock = Deencapsulation.invoke(scraper, "extractProductAvailability", doc.getAllElements());
		Assert.assertEquals(inStock, Boolean.TRUE);
	}

	@Test
	public void testExtractProductAvailabilityNotInStock() {
		String html = "<div class=\"product-shop\"><div class=\"extra-info\">"
				+ "<p class=\"availability in-stock\"><span class=\"label\">"
				+ "Availability:</span> <span class=\"value\">Out of stock</span></p></div></div>";
		Document doc = Jsoup.parse(html);
		SliScraper scraper = new SliScraper(new ProductScrapeRequest("/someTestUrl"));
		Boolean inStock = Deencapsulation.invoke(scraper, "extractProductAvailability", doc.getAllElements());
		Assert.assertEquals(inStock, Boolean.FALSE);
	}

	@Test
	public void testExtractProductAvailabilityFailure() {
		String html = "<div class=\"product-shop\"><div class=\"extra-info\">"
				+ "<p class=\"availabilit in-stock\"><span class=\"label\">"
				+ "Availability:</span> <span class=\"value\">Out of stock</span></p></div></div>";
		Document doc = Jsoup.parse(html);
		SliScraper scraper = new SliScraper(new ProductScrapeRequest("/someTestUrl"));
		Boolean inStock = Deencapsulation.invoke(scraper, "extractProductAvailability", doc.getAllElements());
		Assert.assertEquals(inStock, null);
	}

	@Test
	public void testExtractPricingOldAndDiscount() {
		String html = "<div class=\"product-shop\"><div class=\"price-info\">" + "<div class=\"price-box\">"
				+ "<p class=\"old-price\"><span class=\"price-label\">Regular Price:</span> <span id=\"old-price-403\" class=\"price\"> $175.00 </span></p>"
				+ "<p class=\"special-price\"><span class=\"price-label\">Special Price</span> <span id=\"product-price-403\" class=\"price\"> $140.00 </span></p>"
				+ "</div>" + "</div> </div>";
		Document doc = Jsoup.parse(html);
		SliScraper scraper = new SliScraper(new ProductScrapeRequest("/someTestUrl"));
		ProductPricing pricing = Deencapsulation.invoke(scraper, "extractPricing", doc.getAllElements());
		Assert.assertNotNull(pricing);
		Assert.assertNotNull(pricing.getRegularPrice());
		Assert.assertNotNull(pricing.getDiscountedPrice());
		Assert.assertNull(pricing.getFromPrice());
		Assert.assertNull(pricing.getToPrice());
		Assert.assertNull(pricing.getConfiguredPrice());
		Assert.assertEquals(pricing.getRegularPrice(), "$175.00");
		Assert.assertEquals(pricing.getDiscountedPrice(), "$140.00");
	}

	@Test
	public void testExtractPricingOldAndDiscountFailure() {
		String html = "<div class=\"product-shop\"><div class=\"price-info\">" + "<div class=\"price-bo\">"
				+ "<p class=\"old-price\"><span class=\"price-label\">Regular Price:</span> <span id=\"old-price-403\" class=\"price\"> $175.00 </span></p>"
				+ "<p class=\"special-price\"><span class=\"price-label\">Special Price</span> <span id=\"product-price-403\" class=\"price\"> $140.00 </span></p>"
				+ "</div>" + "</div> </div>";
		Document doc = Jsoup.parse(html);
		SliScraper scraper = new SliScraper(new ProductScrapeRequest("/someTestUrl"));
		ProductPricing pricing = Deencapsulation.invoke(scraper, "extractPricing", doc.getAllElements());
		Assert.assertNotNull(pricing);
		Assert.assertNull(pricing.getRegularPrice());
		Assert.assertNull(pricing.getDiscountedPrice());
		Assert.assertNull(pricing.getFromPrice());
		Assert.assertNull(pricing.getToPrice());
		Assert.assertNull(pricing.getConfiguredPrice());
	}

	@Test
	public void testExtractPricingFromAndTo() {
		String html = "<div class=\"product-shop\"><div class=\"price-info\"><div class=\"price-box\"><p class=\"price-from\"> <span class=\"price-label\">From:</span> <span class=\"price\">$185.00</span></p><p class=\"price-to\"> <span class=\"price-label\">To:</span> <span class=\"price\">$275.00</span></p></div> "
				+ "<div class=\"price-box\"><p class=\"price-as-configured\"> <span class=\"price-label\">Price as configured:</span> <span class=\"full-product-price\"> <span class=\"price\" id=\"product-price-446\"> $0.00 </span> </span> </p> "
				+ " </div></div></div>";
		Document doc = Jsoup.parse(html);
		SliScraper scraper = new SliScraper(new ProductScrapeRequest("/someTestUrl"));
		ProductPricing pricing = Deencapsulation.invoke(scraper, "extractPricing", doc.getAllElements());
		Assert.assertNotNull(pricing);
		Assert.assertNull(pricing.getRegularPrice());
		Assert.assertNull(pricing.getDiscountedPrice());
		Assert.assertNotNull(pricing.getFromPrice());
		Assert.assertNotNull(pricing.getToPrice());
		Assert.assertNotNull(pricing.getConfiguredPrice());
		Assert.assertEquals(pricing.getFromPrice(), "$185.00");
		Assert.assertEquals(pricing.getToPrice(), "$275.00");
		Assert.assertEquals(pricing.getConfiguredPrice(), "$0.00");
	}

	@Test
	public void testExtractPricingFromAndToFailure() {
		String html = "<div class=\"product-shop\"><div class=\"price-info\"><div class=\"price-box\"><p class=\"price-from\"> <span class=\"price-label\">From:</span> <span class=\"price\">$185.00</span></p><p class=\"price-to\"> <span class=\"price-label\">To:</span> <span class=\"price\">$275.00</span></p></div> "
				+ "<div class=\"price-box\"><p class=\"price-as-configure\"> <span class=\"price-label\">Price as configured:</span> <span class=\"full-product-price\"> <span class=\"price\" id=\"product-price-446\"> $0.00 </span> </span> </p> "
				+ " </div></div></div>";
		Document doc = Jsoup.parse(html);
		SliScraper scraper = new SliScraper(new ProductScrapeRequest("/someTestUrl"));
		ProductPricing pricing = Deencapsulation.invoke(scraper, "extractPricing", doc.getAllElements());
		Assert.assertNotNull(pricing);
		Assert.assertNull(pricing.getRegularPrice());
		Assert.assertNull(pricing.getDiscountedPrice());
		Assert.assertNotNull(pricing.getFromPrice());
		Assert.assertNotNull(pricing.getToPrice());
		Assert.assertNull(pricing.getConfiguredPrice());
		Assert.assertEquals(pricing.getFromPrice(), "$185.00");
		Assert.assertEquals(pricing.getToPrice(), "$275.00");
	}

	@Test
	public void testExtractCategoryBreadcrumbsSuccess() {
		String html = "<div class=\"breadcrumbs\"><ul><li class=\"home\"> <a href=\"http://www.sli-demo.com/\" title=\"Go to Home Page\">Home</a><span>/</span></li><li class=\"product\"> <strong>MP3 Player with Audio</strong> </li></ul></div>";
		Document doc = Jsoup.parse(html);
		SliScraper scraper = new SliScraper(new ProductScrapeRequest("/someTestUrl"));
		List<String> categorys = Deencapsulation.invoke(scraper, "extractCategoryBreadcrumbs", doc);
		Assert.assertNotNull(categorys);
		Assert.assertEquals(categorys.size(), 2);
		Assert.assertEquals(categorys.get(0), "Home");
		Assert.assertEquals(categorys.get(1), "MP3 Player with Audio");
	}

	@Test
	public void testExtractCategoryBreadcrumbsFailure() {
		String html = "<div class=\"breadcrumb\"><ul><li class=\"home\"> <a href=\"http://www.sli-demo.com/\" title=\"Go to Home Page\">Home</a><span>/</span></li><li class=\"product\"> <strong>MP3 Player with Audio</strong> </li></ul></div>";
		Document doc = Jsoup.parse(html);
		SliScraper scraper = new SliScraper(new ProductScrapeRequest("/someTestUrl"));
		List<String> categorys = Deencapsulation.invoke(scraper, "extractCategoryBreadcrumbs", doc);
		Assert.assertNotNull(categorys);
		Assert.assertEquals(categorys.size(), 0);
	}

	@Test
	public void testExtractDescriptionSuccess() {
		String expectedOutput = "Includes a choice between our Compact MP3 player or our Digital Media Player and Earbuds or Headphones.";
		String html = "<dt class=\"tab\">Description</dt><dd class=\"tab-container\"><div class=\"tab-content\"><h2>Details</h2>"
				+ "<div class=\"std\">Includes a choice between our Compact MP3 player or our Digital Media Player and Earbuds or Headphones.</div></div></dd>";
		Document doc = Jsoup.parse(html);
		SliScraper scraper = new SliScraper(new ProductScrapeRequest("/someTestUrl"));
		String desc = Deencapsulation.invoke(scraper, "extractProductDescription", doc);
		Assert.assertNotNull(desc);
		Assert.assertEquals(desc, expectedOutput);
	}

	@Test
	public void testExtractDescriptionFailure() {
		String html = "<dt class=\"tab\">Description</dt><dd class=\"tab-container\"><div class=\"tab-content\"><h2>Details</h2>"
				+ "<div class=\"stde\">Includes a choice between our Compact MP3 player or our Digital Media Player and Earbuds or Headphones.</div></div></dd>";
		Document doc = Jsoup.parse(html);
		SliScraper scraper = new SliScraper(new ProductScrapeRequest("/someTestUrl"));
		String desc = Deencapsulation.invoke(scraper, "extractProductDescription", doc);
		Assert.assertNull(desc);
	}

	@Test
	public void testExtractProductAttributesSuccess() {
		String html = "<dt class=\"tab\">Additional Information</dt><dd class=\"tab-container\"><div class=\"tab-content\"><h2>Additional Information</h2><table id=\"product-attribute-specs-table\" class=\"data-table\"><colgroup> <col width=\"25%\" /> <col /> </colgroup><tbody><tr>"
				+ "<th class=\"label\">Occasion</th><td class=\"data\">Evening</td></tr><tr><th class=\"label\">Type</th><td class=\"data\">Shirts</td>"
				+ "</tr><tr><th class=\"label\">Gender</th>"
				+ "<td class=\"data\">Male</td></tr></tbody></table></div></dd>";
		Document doc = Jsoup.parse(html);
		SliScraper scraper = new SliScraper(new ProductScrapeRequest("/someTestUrl"));
		Map<String, String> attr = Deencapsulation.invoke(scraper, "extractProductAttributes", doc);
		Assert.assertNotNull(attr);
		Assert.assertEquals(attr.size(), 3);
		Assert.assertEquals(attr.containsKey("Occasion"), true);
		Assert.assertEquals(attr.containsKey("Gender"), true);
		Assert.assertEquals(attr.containsKey("Type"), true);
		Assert.assertEquals(attr.get("Type"), "Shirts");
		Assert.assertEquals(attr.get("Gender"), "Male");
		Assert.assertEquals(attr.get("Occasion"), "Evening");
	}

	@Test
	public void testExtractProductAttributesFailure() {
		String html = "<dt class=\"tab\">Additional Information</dt><dd class=\"tab-container\"><div class=\"tab-content\"><h2>Additional Information</h2><table id=\"product-attribute-specs-tabl\" class=\"data-table\"><colgroup> <col width=\"25%\" /> <col /> </colgroup><tbody><tr>"
				+ "<th class=\"label\">Occasion</th><td class=\"data\">Evening</td></tr><tr><th class=\"label\">Type</th><td class=\"data\">Shirts</td>"
				+ "</tr><tr><th class=\"label\">Gender</th>"
				+ "<td class=\"data\">Male</td></tr></tbody></table></div></dd>";
		Document doc = Jsoup.parse(html);
		SliScraper scraper = new SliScraper(new ProductScrapeRequest("/someTestUrl"));
		Map<String, String> attr = Deencapsulation.invoke(scraper, "extractProductAttributes", doc);
		Assert.assertNotNull(attr);
		Assert.assertEquals(attr.size(), 0);
	}

	@Test
	public void testExtractProductInfo() {
		SliScraper scraper = new SliScraper(new ProductScrapeRequest("someTestUrl"));

		new MockUp<SliScraper>() {

			@Mock
			public Document getDocument(String url) throws IOException {
				String html = "<div class=\"product-shop\"><div class=\"short-description\">"
						+ "<div class=\"std\">A bold hue and understated dobby detail bring refined nuance "
						+ "to this modern dress shirt.</div> </div></div>";
				Document doc = Jsoup.parse(html);
				return doc;
			}

			@Mock
			public String extractProductName(Elements productInfoTopDiv) {
				return "testProduct";
			}

			@Mock
			public Boolean extractProductAvailability(Elements productInfoTopDiv) {
				return false;
			}

			@Mock
			public String extractShortDescription(Elements productInfoTopDiv) {
				return "testShortDesc";
			}

			@Mock
			public ProductPricing extractPricing(Elements productInfoTopDiv) {
				return new ProductPricing();
			}

			@Mock
			public List<String> extractCategoryBreadcrumbs(Document document) {
				List<String> result = new ArrayList<>();
				result.add("Home");
				result.add("testCat1");
				result.add("testCat2");
				return result;
			}

			@Mock
			public String extractProductDescription(Document document) {
				return "testDesc";
			}

			@Mock
			public Map<String, String> extractProductAttributes(Document document) {
				Map<String, String> result = new HashMap<>();
				result.put("attr1", "value1");
				result.put("attr2", "value2");
				return result;
			}
		};

		ProductScrapeResponse response = scraper.extractProductInfo();
		System.out.println(response);
		Assert.assertNotNull(response.getProduct());

		Assert.assertNotNull(response.getProduct().getName());
		Assert.assertEquals(response.getProduct().getName(), "testProduct");

		Assert.assertNotNull(response.getProduct().getShortDescription());
		Assert.assertEquals(response.getProduct().getShortDescription(), "testShortDesc");
	}

}
