Write code to extract the information from each product page that we require to make the product index.

Output Format:
The output should be plain text in the format shown below and each item should have no newlines, return codes, or html tags in it.

* Title: xxxxx yyyy zzzzz
* Price: $1234.56
* Brand: XXXYYYZZZ
... etc... repeat for each item
----------------------------------------------------------------------------------------------------------------------
## Solution 

* Steps for code checkout, build and mock execution - 
	1. git clone https://pratsprats@bitbucket.org/slideng101/sli-dev-prateeks.git
	2. mvn clean package
	3. mvn exec:java -Dexec.mainClass="com.sli.scraper.runner.MockSliScrapeRunner"

* Steps for generating code coverage report - 
	1. Run "mvn clean cobertura:cobertura" and check report at location - target/site/cobertura/index.html

* MockSliScrapeRunner is a test class created for running SLI scraper for the list of urls stored in static file at location src/main/resources/file/sli-demo-urls.txt

* Sample Output - 

	* Url - http://www.sli-demo.com/home-decor/bed-bath/park-row-throw.html 
	* Product Name - Park Row Throw 
	* Short Description - A rustic wool blend leaves our Park Row Throw feeling lofty and warm. Packs perfectly into carry-ons. 
	* Description - Woven acrylic/wool/cotton. 50" x 75". Spot clean. 
	* Category - [Home, Park Row Throw] 
	* In Stock - true 
	* Attributes - {Color=Blue, Decor Type=Throw, Material=Wool, Bed & Bath Type=Bed} 
	* Regular Price - $240.00 
	* Discounted Price - $120.00
	* Scrape start time - Sun Dec 10 21:12:34 IST 2017 
	* Scrape end time - Sun Dec 10 21:12:40 IST 2017 
	------------------------------------------------------------------------ 
	* Url - http://www.sli-demo.com/home-decor/electronics/mp3-player-with-audio.html 
	* Product Name - MP3 Player with Audio 
	* Short Description - Pick up your Media Player and Audio Output together. 
	* Description - Includes a choice between our Compact MP3 player or our Digital Media Player and Earbuds or Headphones. 
	* Category - [Home, MP3 Player with Audio] 
	* In Stock - true 
	* From Price - $185.00 
	* To Price - $275.00 
	* Configured Price - $0.00 
	* Scrape start time - Sun Dec 10 21:12:35 IST 2017 
	* Scrape end time - Sun Dec 10 21:12:44 IST 2017

* IMPORTANT - While working on this exercise, I noticed that html content of few url's were different when rendered on browser vs when downloaded using code. After debugging, I realized that it's because of a cookie named "frontend" used in header of all requests. When I hard coded browser cookie in my code, html content changed. But this cookie expires every hour. In class, SliScraper.java, I have hardcoded this cookie for testing purpose. Please ignore that. 

